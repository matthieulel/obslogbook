import * as React from 'react';
import {Table} from 'antd';
import moment from 'moment';
import {Button} from 'react-bootstrap';
import 'antd/dist/antd.css';
import "../css/displayTable.css";
/**
This class is an element of search components (display table with results)
*/
class displayTable extends React.Component{
    constructor(props){
        super(props);
        this.state={
            listObs:[],
            displayObs:props.displayObs,
            modify:props.modify
        }
    }
    
    render(){
        var title=window["translations"]().title;
        var button=window["translations"]().button;
        if(this.state.listObs!==undefined){
            this.state.listObs.forEach((item,k)=>{
                item["key"]=k++;
            });
        }
        
    // Constant of generics columns disposition
    const columns = [
            {
                title: title.action,
                key: 'action',
                render: (text, record) => (
                        <span key={record.observationId}>
                                <Button type="primary" id="display" className="changeLang purple-bg" onClick={() => { this.state.displayObs(record) }}>{button.display}</Button>
                        </span>
                ),
            },
            {
                title: title.observation_id,
                dataIndex: 'observation_id',
                key: 'observation_id',
            },
            {
                title: title.observation_type,
                dataIndex: 'observation_type',
                key: 'observation_type',
                render:(record)=>( <span>{title[record]}</span>),
            },
            {
                title: title.date,
                dataIndex: 'date',
                key: 'date',
                render:(date)=>(moment(date)).format("YYYY/MM/DD"), //set the text for this columns here we use moment to format our date (YYYY/MM/DD)
                sorter: (a, b) => moment(a.date).unix() - moment(b.date).unix(), //Use to
    
    
            },
            {
                title: title.start_time,
                dataIndex: ["place",'start_time'],
                key: 'place.start_time',
    
            },
            {
                title:  title.end_time,
                dataIndex: ["place","end_time"],
                key: 'place.end_time',
    
            },
            {
                title:  title.optic_name,
                dataIndex: ["main_optic","optic_name"],
                key: "main_optic.optic_name",
            },
            {
                title:  title.ccd_name,
                dataIndex: ["main_optic","ccd_name"],
                key: 'main_optic.ccd_name',
    
            },
            {
                title:  title.ra_target_j2000,
                dataIndex: ["target","ra_target_j2000"],
                key: "target.ra_target_j2000",
            },
            {
                title:  title.dec_target_j2000,
                dataIndex: ["target","dec_target_j2000"],
                key: "target.dec_target_j2000",
    
            },
            {
                title:  title.object_name,
                dataIndex: ["target","object_name"],
                key: "target.object_name",
    
            },
            {
                title:  title.target_type,
                dataIndex: ["target","target_type"],
                key: "target.target_type",
                render:(record)=>( <span>{title[record]}</span>),
    
            },
            {
                title:  title.type_caracteristics,
                dataIndex: ["target","type_caracteristics"],
                key: "target.type_caracteristics",
    
            },
            {
                title: title.action,
                key: 'actionmodify',
                render: (text, record) => (
                        <span key={record.observationId}>
                                <Button type="primary" id="modify" className="changeLang purple-bg" onClick={() => { this.state.modify(record) }}>{button.modify}</Button>
                        </span>
                ),
            },
        ]
        return( 
            <div className="obsList">
            { this.state.listObs.length >0 &&
                <div className="antdTable">
                    <Table dataSource={this.state.listObs} pagination={{ pageSize: 5 }} columns={columns} scroll={{ x: 100}}/>
                </div>
            }
            </div>
            );
    }
}export default displayTable;

