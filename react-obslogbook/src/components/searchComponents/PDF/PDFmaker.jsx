import React from 'react';
import { Page, Text, View, Document} from '@react-pdf/renderer';
import title from "../../dataJson/en/title_en.json";
import HeaderPDF from './headerPDF';
import {StyleSheet} from '@react-pdf/renderer';

// Constant of styles used in the PDF file
const styles=StyleSheet.create({
    page: {
        padding: 20,
    },
    sectionHuge: {
        flexDirection: 'row'
    },
    sectionCol: {
        flexDirection: 'col'
    },
    sectionLittle: {
        fontSize: 10,
         margin: 5,
          padding: 10,
        flexGrow: 1,
        width: '50%', 
    },
    subtitle: {
        fontSize: 12,
        margin: 12
    },
    title: {
        fontSize: 24,
        textAlign: 'center'
    }
});
/**
 * ObservationResult : return a PDF Document with all datas of the observation
 */
export default class PDFmaker extends React.Component {

    constructor(props) {
            super(props);
            console.log(props.addons)
            this.key=0;
            var displayIntro = false;
            var displayConcl = false;
            if(props.addons[0] != null){
                displayIntro = true;
            }
            if(props.addons[1] != null){
                displayConcl = true;
            }
            this.state = {
                obsMode:props.observation.observation_type,
                styles:styles,
                addons: props.addons,
                displayIntro: displayIntro,
                displayConcl: displayConcl,
                // Category used by observation type
                allcategories:{
                    spectro:{
                        environment:["_id","date","meteo","observation_id","observation_type","place"],
                        equipment:["mount","main_optic","guide_optic","spectrograph"],
                        acquisition:["target","target_calib","target_dark","target_flats","target_image","target_offsets","ref_star_object"],
                        result:["result","process_software","result_path","url_publication","informations"]
                    },
                    astro:{
                        environment:["_id","date","meteo","observation_id","observation_type","place"],
                        equipment:["mount" ,"main_optic","guide_optic"],
                        acquisition:["target","target_calib","target_dark","target_flats","target_image","target_offsets"],
                        result:["result","process_software","result_path","url_publication","informations"]
                    },
                    visual:{
                        environment:["_id","date","meteo","observation_id","observation_type","place"],
                        equipment:["mount" ,"main_optic","guide_optic"],
                        acquisition:["target","ref_star_object","observation_comments","dec_target_j2000","object_name","ra_target_j2000","target_type","type_caracteristics"],
                        result:["result","process_software","result_path","url_publication","informations"]
                    }
                }
            }
            console.log(this.state.displayIntro +" "+ this.state.displayConcl);
            this.state.categories=this.state.allcategories[this.state.obsMode];
    }

    /**
     * renderFunction: return all Observation datas in PDF Format
     * @param {*} obj , all datas of one observation
     * @param {*} padding , padding of the datas
     *  @param {*} field , the field required
     */
    renderFunction=(obj,padding,field)=>{
        var ret=[];
        // Verification if the file is not undefined
        if (field===undefined){
            return;
        }
        // For each value in the observation, the name of the category and it value is added in the PDF file
        Object.keys(obj).forEach(info=>{
            // Verification if the category is not empty
            if(obj[info] !== ""){
                // Verification if the category is used in this type of observation
                if(field===true || this.state.categories[field].includes(info)){
                    ret.push(
                        <Text key={this.key++}>											
                            {typeof(obj[info])==="string"&&<Text key={this.key++}>{title[info]!==undefined?title[info]:info} : </Text>}
                            {typeof(obj[info])==="string"?
                                <Text key={this.key++}>{obj[info]}</Text>:ret.push(<Text key={this.key++} className="font-weight-bold mt-2">{title[info]!==undefined?this.renderFunction(obj[info],padding+2,true).length!==0&&title[info]+" :":info+" :"}</Text>)&&
                                this.renderFunction(obj[info],padding+2,true).forEach((x)=>{
                                ret.push(x)})
                            }
                        </Text>);
                }
            }
        })
        return ret;
    }
    
     /**
     * renderFunction: return all Observation datas in PDF Format but with a flat object in entry
     * @param {*} obj , all datas of one observation as flat
     *  @param {*} field , the field required
     */
    renderFunctionFlat=(obj,field)=>{
        var ret=[];
        let Fields=require('../../classes/fields').Fields;
        var cat=[];
        switch(field){
            case "environment":
                cat=Fields.getEnvironmentFields();
                break;
            case "equipment":
                cat=Fields.getEquipmentFields();
                break;
            case "acquisition":
                cat=Fields.getAcquisitionFields();
                break;
            case "result":
                cat=Fields.getResultFields();
                break;
            default:
                return;
        }
        Object.keys(obj).forEach(info=>{
            if(obj[info] !== ""){
                if(cat.includes(info)){
                    ret.push(
                        <Text key={this.key++}>											
                            {typeof(obj[info])==="string"&&<Text key={this.key++}>{title[info]!==undefined?title[info]:info} : </Text>}
                            <Text key={this.key++}>{obj[info]}</Text>
                        </Text>);
                }
            }
        })
        return ret;
    }

    render(){
        return (this.props.flatMode===true?
            <Document>
                <Page size="A4" style={this.state.styles.page}>
                    <HeaderPDF
                        type={window["translations"]().title[this.state.obsMode]}
                        nameObject={this.props.observation["object_name"]}
                        decTarget={this.props.observation["dec_target_j2000"]}
                        raTarget={this.props.observation["ra_target_j2000"]}
                    />
                    { this.state.displayIntro &&
                        <View style={this.state.styles.sectionCol}>
                        <View style={this.state.styles.sectionLittle}>
                            <Text style={this.state.styles.subtitle}>{window["translations"]().title.introduction}</Text>
                            <Text key={this.key++}>{this.state.addons[0]}</Text>
                        </View>
                        </View>
                    }
                    <View style={this.state.styles.sectionHuge}>
                        <View style={this.state.styles.sectionLittle}>
                            <Text style={this.state.styles.subtitle}>Environnement</Text>
                            {this.renderFunctionFlat(this.props.observation,"environment")}
                            <Text style={this.state.styles.subtitle}>Equipment</Text>
                            {this.renderFunctionFlat(this.props.observation,"equipment")}
                        </View>
                        <View style={this.state.styles.sectionLittle}>
                            <Text style={this.state.styles.subtitle}>Acquisition</Text>
                            {this.renderFunctionFlat(this.props.observation,"acquisition")}
                            <Text style={this.state.styles.subtitle}>Results</Text>
                            {this.renderFunctionFlat(this.props.observation,"result")}
                        </View>
                    </View>
                    { this.state.displayConcl &&
                        <View style={this.state.styles.sectionCol}>
                            <View style={this.state.styles.sectionLittle}>
                                <Text style={this.state.styles.subtitle}>{window["translations"]().title.conclusion}</Text>
                                <Text key={this.key++}>{this.state.addons[1]}</Text>
                            </View>
                        </View>
                    }
                </Page>
            </Document>
            :
            <Document>
                <Page size="A4" style={this.state.styles.page}>
                    <HeaderPDF
                        type={window["translations"]().title[this.state.obsMode]}
                        nameObject={this.props.observation["target"]["object_name"]}
                        decTarget={this.props.observation["target"]["dec_target_j2000"]}
                        raTarget={this.props.observation["target"]["ra_target_j2000"]}
                    />
                    {this.state.displayIntro &&
                        <View style={this.state.styles.sectionCol}>
                            <View style={this.state.styles.sectionLittle}>
                                <Text style={this.state.styles.subtitle}>{window["translations"]().title.introduction}</Text>
                                <Text key={this.key++}>{this.state.addons[0]}</Text>
                            </View>
                        </View>
                    }
                    <View style={this.state.styles.sectionHuge}>
                        <View style={this.state.styles.sectionLittle}>
                            <Text style={this.state.styles.subtitle}>Environnement</Text>
                            {this.renderFunction(this.props.observation,0,"environment")}
                            <Text style={this.state.styles.subtitle}>Equipment</Text>
                            {this.renderFunction(this.props.observation,0,"equipment")}
                        </View>
                        <View style={this.state.styles.sectionLittle}>
                            <Text style={this.state.styles.subtitle}>Acquisition</Text>
                            {this.renderFunction(this.props.observation,0,"acquisition")}
                            <Text style={this.state.styles.subtitle}>Results</Text>
                            {this.renderFunction(this.props.observation,0,"result")}
                        </View>
                    </View>
                    { this.state.displayConcl &&
                            <View style={this.state.styles.sectionCol}>
                                <View style={this.state.styles.sectionLittle}>
                                    <Text style={this.state.styles.subtitle}>{window["translations"]().title.conclusion}</Text>
                                    <Text key={this.key++}>{this.state.addons[1]}</Text>
                                </View>
                            </View>
                    }
                </Page>
            </Document>);
    }
}