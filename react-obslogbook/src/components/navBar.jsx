import * as React from 'react';
import {Nav,Navbar,NavDropdown,Image} from'react-bootstrap';
import './css/navBar.css';
import en from "../resources/gb.svg"
import fr from "../resources/fr.svg"
import es from "../resources/es.svg"
import logo from "../resources/logoOLB.png";

//On the click of one of the buttons, we execute the function that has been passed through react props
//So the function is executed in the environment of App.js, his parent.
class NavBar extends React.Component {
    constructor(props){
        super(props);
        this.state={
            status:"searchButton",
            statua:"alertButton"
        }
    }
    /** This function set the status for button search / Back to observation */
    setStatusExterne=(stat)=>{
        this.setState({status:stat});
    }


    /** This componentDidMount is used for the scroll animation fading on navbar */
    componentDidMount(){
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            var navBar = document.getElementById("navBarCustom");
            var navBarClassic = document.getElementById("basic-navbar-nav");
            var navBarToggle=document.getElementById("toggle-navbar");
            if(currentScrollPos<50){
                navBar!==null?navBar.style.marginTop = "0":console.log();
                return;
            }
            if (prevScrollpos > currentScrollPos) {
                navBar!==null?navBar.style.marginTop = "0":console.log();
            } 
            else {
                navBarClassic!==null?navBarClassic.classList.remove("show"):console.log();
                navBarToggle!==null?navBarToggle.classList.add("collapsed"):console.log();
                navBar!==null?navBar.style.marginTop = "-200px":console.log();
            }
            prevScrollpos = currentScrollPos;
        }
    }

    /**
     * obsearch : change the name of the back button depending on whether it is on the search page or on the observation page
     */
    obsearch=()=>{
        this.props.handleSearchVisible();   
        this.state.status==="searchButton"?this.setState({status:"homeButton"}):this.setState({status:"searchButton"});
        this.state.statua==="homeButton"?this.setState({statua:"alertButton"}):this.setState({statua:"alertButton"});                  
    }

     /**
     * obsearch : change the name of the back button depending on whether it is on the search page or on the observation page
     */
      obalert=()=>{
        this.props.handleAlertVisible();   
        this.state.statua==="alertButton"?this.setState({statua:"homeButton"}):this.setState({statua:"alertButton"});
        this.state.status==="homeButton"?this.setState({status:"searchButton"}):this.setState({status:"searchButton"});                    
    }

    render(){
        var language_button=window["translations"]().button;
        var language={"en":en,"fr":fr, "es":es};
        var flag=language[this.props.language];
        return (
            <Navbar id ="navBarCustom" className="navbar-dark whole-nav" expand="lg">
                <Navbar.Brand id="megaTitle"href="/"><img src={logo} alt="logo" style={{width:55, marginTop: -7}}/><span>ObsLogBook - Development version</span></Navbar.Brand >
                    <Navbar.Toggle aria-controls="basic-navbar-nav" id="toggle-navbar" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link className={"btn btn-primary mr-2 text-white"}
                                    onClick={this.obsearch} id="obsearch" >{language_button[this.state.status]}
                                </Nav.Link>
                                <Nav.Link className={"btn btn-primary mr-2 text-white"} onClick={ this.props.handleWarningsNewSessionPopup}>{ language_button["newSession"]}</Nav.Link>
                                <Nav.Link className={"btn btn-primary mr-2 text-white"} onClick={ this.props.handleWarningsNewObservationPopup}>{ language_button["newObservation"]}</Nav.Link>
                                <Nav.Link className={"btn btn-warning mr-2 text-white"} onClick={ this.props.handleSettingsPopup}>{ language_button["settings"]}</Nav.Link>
                                <Nav.Link className={"btn btn-warning mr-2 text-white"}
                                    onClick={this.obalert} id="obalert">{language_button[this.state.statua]}
                                </Nav.Link>
                                <NavDropdown className="droptest" title={<Image className="imagenav" src={flag} alt="flag"></Image>}>
                                    <NavDropdown.Item onClick={({target})=>{this.props.setLang(target.id)}} className="w-50" id="en"><Image id="en" className="imagenav" src={en} alt="en"></Image></NavDropdown.Item>
                                    <NavDropdown.Item onClick={({target})=>{this.props.setLang(target.id)}} className="w-50" id="fr"><Image id="fr" className="imagenav" src={fr} alt="fr"></Image></NavDropdown.Item>
                                    <NavDropdown.Item onClick={({target})=>{this.props.setLang(target.id)}} className="w-50" id="es"><Image id="es" className="imagenav" src={es} alt="es"></Image></NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>
            </Navbar>

        );
    }
}export default NavBar;
