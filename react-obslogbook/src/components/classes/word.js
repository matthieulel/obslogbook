import { Document, Packer, Paragraph, TextRun, HeadingLevel, PageBreak, PageNumber, Footer, AlignmentType, Header,Media} from "docx";
import { saveAs } from "file-saver";
import imageBase64Data from '../../resources/logo64.json'


/**
 * createWord : create a Word file with the observation's data
 * @param fileName , the name of the Word file
 */
var Fields=require("./fields").Fields;
var title=window["translations"]().title;
export default async function createWord(observation){
    //Create document
    const doc = new Document();
    
    var fileName=observation.observation_id;

    const image = Media.addImage(doc,Uint8Array.from(atob(imageBase64Data.image), c => c.charCodeAt(0)),100,100);

    var header = buildHearder(image,observation);

    var env = new TextRun({
        text: title['environment'],
        color: "6A57D3",
        font: "Calibri"
    });
    var headingEnv = new Paragraph({
        children: [env],
        heading: HeadingLevel.HEADING_1,
        thematicBreak: true,
    });
    var environnement = buildEnvironement(observation);

    var ekip = new TextRun({
        text: title['equipment'],
        color: "6A57D3",
        font: "Calibri"
    });
    var headingEkip = new Paragraph({
        children: [ekip],
        heading: HeadingLevel.HEADING_1,
        thematicBreak: true,
    });
    var equipement = buildEquipment(observation);

    var acq = new TextRun({
        text: title['acquisition'],
        color: "6A57D3",
        font: "Calibri"
    });
    var headingAcq = new Paragraph({
        children: [acq],
        heading: HeadingLevel.HEADING_1,
        thematicBreak: true,
    });
    var acquisition = buildAcquisition(observation);

    var res = new TextRun({
        text: title['result'],
        color: "6A57D3",
        font: "Calibri"
    });
    var headingRes = new Paragraph({
        children: [res],
        heading: HeadingLevel.HEADING_1,
        thematicBreak: true,
    })
    var result = buildResult(observation);

    var footer = buildFooter();

    doc.addSection({
        headers: {
            default: new Header({
                children: [header],
            }),
        },
        footers: {
            default: new Footer({
                children: [footer]
            })
        },
        children: [
            headingEnv,
            environnement,
            headingEkip,
            equipement,
            headingAcq,
            acquisition,
            headingRes,
            result],
    });

    //Used to export the file into a .docx file
    Packer.toBlob(doc).then((blob) => {
        //saveAs from FileSaver will download the file
        saveAs(blob, fileName + '.docx');
    });
}

/**
 * buildHeader : build the header of the Word file
 */
let buildHearder = (image,observation) => {
    var headData = [];
    var name;
    var ra;
    var dec;
    if(observation.target!==undefined){
        name=observation['target']['object_name'];
        ra="RA : " + observation['target']['ra_target_j2000'];
        dec="DEC : " + observation['target']['dec_target_j2000'];
    }
    else{
        name=observation['object_name']!==undefined?observation['object_name']:"";
        ra=observation['ra_target_j2000']!==undefined?"RA : "+observation['ra_target_j2000']:"";;
        dec=observation['dec_target_j2000']!==undefined?"DEC : "+observation['dec_target_j2000']:"";;
    }
    
    headData.push(image)
    headData.push(new TextRun({ text: name, size: 50, allCaps: true, font: "Calibri" }));
    headData.push(new TextRun({ text: ra , bold: true, break: 1, font: "Calibri" }));
    headData.push(new TextRun({ text: dec , bold: true, break: 1, font: "Calibri" }));
    headData.push(new TextRun({ text: "\t\t\t\t\t\t\t\t\t\tObsLogBook", bold: true, font: "Calibri" }));
    var header = new Paragraph({
                children: headData
    });
    return header;
}

/**
 * buildFooter : build the footer of the Word file
 */
let buildFooter = () => {
    var footData = [];
    footData.push(new TextRun({
        children: ["Page ", PageNumber.CURRENT],
        font: "Calibri"
    }));

    var footer = new Paragraph({
        children: footData,
        alignment: AlignmentType.RIGHT,
        border: {
            top: {
                color: "6A57D3",
                space: 1,
                value: "single",
                size: 6,
            }
        }
    });
    return footer;
}

/**
 * buildEnvironement : build the environnment section of the Word file
 */
let buildEnvironement = (observation) => {
    var envData = [];
    var objenvData = [];
    console.log();
    console.log(Fields.getEnvironmentFieldsObject());
    Object.keys(observation).forEach(data => {
        if (observation[data] !== "") {
            if (data === '_id' || Fields.getEnvironmentFields().includes(data) ) {
                if (typeof(observation[data])==='object') {
                    if ( Object.keys(observation[data]).length > 0 ) {
                        objenvData.push(new TextRun({ text: title[data] + ": ", bold: true, break: 1, size: 25, font: "Calibri"}));
                        Object.keys(observation[data]).forEach(d => {
                            if (observation[data][d] !== "") {
                                objenvData.push(new TextRun({ text: "\t" + title[d] + " : ", break: 1, font: "Calibri"}));
                                objenvData.push(new TextRun({ text : observation[data][d], font: "Calibri"}));
                            }
                        });
                    }
                } else {
                    envData.push(new TextRun({ text: title[data] + " : ", break: 1, font: "Calibri"}));
                    envData.push(new TextRun({text : observation[data], font: "Calibri"}));
                }
            }
        }
    });

    objenvData.forEach(data => {
        envData.push(data);
    });

    envData.push(new PageBreak());

    var environnement = new Paragraph({
        children: envData
    })

    return environnement;
}

/**
 * buildEquipment : build the Equipement section of the Word file
 */
let buildEquipment = (observation) => {
    var ekipData = [];

    Object.keys(observation).forEach(data => {
        if (observation[data] !== "") {
            if (Fields.getEquipmentFields().includes(data)) {
                if (typeof(observation[data])==='object') {
                    if ( Object.keys(observation[data]).length > 0 ) {
                        ekipData.push(new TextRun({ text: title[data] + ": ", bold: true, break: 1, size: 25, font: "Calibri"}));
                        Object.keys(observation[data]).forEach(d => {
                            if (observation[data][d] !== "") {
                                ekipData.push(new TextRun({ text: "\t" + title[d] + " : ", break: 1, font: "Calibri"}));
                                ekipData.push(new TextRun({ text: observation[data][d], font: "Calibri"}));
                            }
                        });
                    }
                } else {
                    ekipData.push(new TextRun({ text: title[data] + " : ", break: 1, font: "Calibri"}));
                    ekipData.push(new TextRun({ text: observation[data], font: "Calibri"}));
                }
            }
        }
    });

    ekipData.push(new PageBreak());

    var equipement = new Paragraph({
        children: ekipData
    })

    return equipement;
}

/**
 * buildAcquisition : build the Acquisiiton section of the Word file
 */
let buildAcquisition = (observation) => {
    var acqData = [];

    Object.keys(observation).forEach(data => {
        if (observation[data] !== "") {
            if (Fields.getAcquisitionFields().includes(data)) {
                if (typeof(observation[data])==='object') {
                    if ( Object.keys(observation[data]).length > 0 ) {
                        acqData.push(new TextRun({ text: title[data] + ": ", bold: true, break: 1, size: 25, font: "Calibri"}));
                        Object.keys(observation[data]).forEach(d => {
                            if (observation[data][d] !== ""){
                                acqData.push(new TextRun({ text: "\t" + title[d] + " : ", break: 1, font: "Calibri"}));
                                acqData.push(new TextRun({ text: observation[data][d], font: "Calibri"}));
                            }
                        });
                    }
                } else {
                    acqData.push(new TextRun({ text: title[data] + " : ", break: 1, font: "Calibri"}));
                    acqData.push(new TextRun({ text: observation[data], font: "Calibri"}));
                }
            }
        }
    });

    acqData.push(new PageBreak());

    var acquisition = new Paragraph({
        children: acqData
    });

    return acquisition;
}
/**
 * buildResult : build the Result section of the Word file
 */
let buildResult = (observation) => {
    var resData = [];

    Object.keys(observation).forEach(data => {
        if (observation[data] !== "") {
            if (Fields.getResultFields().includes(data)) {
                if (typeof(observation[data])==='object') {
                    if ( Object.keys(observation[data]).length > 0 ) {
                        resData.push(new TextRun({ text: title[data] + ": ", bold: true, break: 1, size: 25, font: "Calibri"}));
                        Object.keys(observation[data]).forEach(d => {
                            if (observation[data][d] !== "") {
                                resData.push(new TextRun({ text: "\t" + title[d] + " : ", break: 1, font: "Calibri"}));
                                resData.push(new TextRun({ text: observation[data][d], font: "Calibri"}));
                            }
                        });
                    }
                } else {
                    resData.push(new TextRun({ text: title[data] + " : ", break: 1, font: "Calibri"}));
                    resData.push(new TextRun({ text: observation[data], font: "Calibri"}));
                }
            }
        }
    });

    var result = new Paragraph({
        children: resData
    });

    return result;
}

