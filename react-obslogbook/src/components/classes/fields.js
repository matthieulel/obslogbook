import environment from '../dataJson/environment.json';
import acquisition from '../dataJson/acquisition.json';
import equipment from '../dataJson/equipment.json';
import result from '../dataJson/result.json'


export class Fields{
    static environment=environment;
    static acquisition=acquisition;
    static equipment=equipment;
    static result=result;

    static allFieldsList(){
        var fields=[];
        fields=fields.concat(this.getEnvironmentFields());
        fields=fields.concat(this.getEquipmentFields());
        fields=fields.concat(this.getAcquisitionFields());
        fields=fields.concat(this.getResultFields());
        return fields;
    }
    static getEnvironmentFields(){
        var arr=[];
        for(var i in environment.environment){
            var item=environment.environment[i].balise!==undefined?environment.environment[i].name_bdd:environment.environment[i].name;
            if(item!==undefined)arr.push(item);
        }
        return arr;
    }
    static getEquipmentFields(){
        var arr=[];
        for(var i in equipment.equipment){
            var item=equipment.equipment[i].balise!==undefined?equipment.equipment[i].name_bdd:equipment.equipment[i].name;
            if(item!==undefined)arr.push(item);
        }
        return arr;
    }
    static getAcquisitionFields(){
        var arr=[];
        for(var i in acquisition.acquisition){
            var item=acquisition.acquisition[i].balise!==undefined?acquisition.acquisition[i].name_bdd:acquisition.acquisition[i].name;
            if(item!==undefined)arr.push(item);
        }
            
        return arr;
    }
    static getResultFields(){
        var arr=[];
        for(var i in result.result){
            var item=result.result[i].balise!==undefined?result.result[i].name_bdd:result.result[i].name;
            if(item!==undefined)arr.push(item);
        }

        return arr;
    }

    static allFieldsObjectList(){
        var fields=[];
        fields=fields.concat(this.getEnvironmentFieldsObject());
        fields=fields.concat(this.getEquipmentFieldsObject());
        fields=fields.concat(this.getAcquisitionFieldsObject());
        fields=fields.concat(this.getResultFieldsObject());
        return fields;
    }

    static getEnvironmentFieldsObject(){
        var arr=[];
        for(var i in environment.environment){
            var item=environment.environment[i].name_bdd;
            if(item!==undefined)arr.push(item);
        }
        return arr;
    }
    static getEquipmentFieldsObject(){
        var arr=[];
        for(var i in equipment.equipment){
            var item=equipment.equipment[i].name_bdd;
            if(item!==undefined)arr.push(item);
        }
        return arr;
    }
    static getAcquisitionFieldsObject(){
        var arr=[];
        for(var i in acquisition.acquisition){
            var item=acquisition.acquisition[i].name_bdd;
            if(item!==undefined)arr.push(item);
        }
        return arr;
    }
    static getResultFieldsObject(){
        var arr=[];
        for(var i in result.result){
            var item=result.result[i].name_bdd;
            if(item!==undefined)arr.push(item);
        }
        return arr;
    }
}
