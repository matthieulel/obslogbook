import * as React from 'react';
import {InputGroup, FormControl} from'react-bootstrap';
import './css/inputField.css';
import { Col,Row } from 'react-bootstrap';



/** This hook return a formcontrol component from props parameters */
class inputField extends React.Component{
    constructor(props){
        super(props);
        this.mounted=false;
        this.inputList=props.inputList;
        this.state={
            refresh:false,
            value:props.value,
            observation_type:props.observation_type,
            as:"", 
            type:null,
            min:null,
            max:null,
            step:null,
            pattern:null,
            className:"", 
            plaintext:null,
            custom:null,
            required:"",
            readOnly:false
        }
        for(var element in props.input)
            this.state[element]=props.input[element];
    }

    /**
     * When component finish mounting, initialized the input field component
     */
    componentDidMount(){
        this.mounted=true;
        this.constructComponent();
    }

    /**
     * When component update value and observation type refresh the element 
     */
    componentDidUpdate(k,oldState){

        // Verification if the old state value is the same than the current state value
        if(oldState.value!==this.state.value){
            this.constructComponent();
        }
        // Verification if the old state observation type is the same than the current state observation type
        if(oldState.observation_type!==this.state.observation_type){
            this.constructComponent();
        }
    }

    /**
     * Function that create the component props
    */
    constructComponent=()=>{
        if(!this.mounted)return null;
        var as=this.state.as; 
        var type=this.state.type;
        var min=this.state.min;
        var max=this.state.max;
        var step=this.state.step;
        var pattern=this.state.pattern;
        var className=""; 
        var plaintext=this.state.plaintext;
        var custom=this.state.custom;
        var required="";
        
        // Verification if the input field is required for it observation type
        if(this.state.isrequired!==undefined && this.state.isrequired.includes(this.state.observation_type)){
            required="required";
            className+=" required";
        }
        // Verification if the input field
        if(this.state.isrecommended!==undefined && 
            this.state.isrecommended.includes(this.state.observation_type)
            && [undefined," ","",null].includes(this.state.value)){
            className+=" recommanded";
        }
        // Verification if the type of the input field is date
        if(this.state.type==="date"){
            as="input";
            type=this.state.type;
            min=this.inputList.getMinDate();
            step=this.state.step;
            pattern=this.state.pattern;  
            if(this.state.value===undefined){
                this.inputList.setState({date:min});
            }
        }
        // Verification if the type of the input field is select
        else if(this.state.type==="select"){
            as="select";
            custom="custom";
        // Verification if the type of the input field is time
        } else if(this.state.type==="time"){
            as="input";
            type=this.state.type;
            if(this.state.name==="start_time" && this.state.value===undefined){
                var today = new Date(),
                time = (today.getHours()<10?"0"+today.getHours():today.getHours())+ ':' + (today.getMinutes()<10?"0"+today.getMinutes():today.getMinutes());
                this.inputList.setState({[this.state.name]:time})
            }
        }
        // Verification if the type of the input field is text
        else if(this.state.type==="text"){
            as="input"; 
            type=this.state.type;
        // Verification if the type of the input field is number
        }else if(this.state.type==="number"){
            as="input"; 
            type=this.state.type;
            step=this.state.step;
            min=this.state.min;
            max=this.state.max;
        }
        // Verification if the input field just be read only
        if(this.state.readOnly){
            as="input";
            className="pl-2";
            plaintext=true;
        }
        // Verification if the input field is the directory path
        if(this.state.name==='directory_path'){
            var defaultPath="/"+this.inputList.state.session_id.replace("_","")+this.state.observation_type.toString().charAt(0);
            this.inputList.setState({directory_path:defaultPath});
        }
        this.setState({as:as,type:type,className:className,plaintext:plaintext,step:step,min:min,max:max,pattern:pattern,required:required,custom:custom});
    }

    render(){  
        var title=window["translations"]().title;
        var desc=window["translations"]().description;
        // Verification if the input field is include in the observation type and if it's hidden or not
        if(this.state.hide!==undefined && this.state.hide.includes(this.state.observation_type))return null;
        if(!this.mounted) return null;
        if(this.state.balise==="h3")
            return(<h3 key={this.state.name}>{title[this.state.name]!=null?title[this.state.name]:title.errorJSON}</h3>);
        return (<InputGroup className={"mb-2"}>
        <Row noGutters className={"w-100"} lg={12} md={12}>
            <Col className={"d-flex"} lg={6} md={6} >
        <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">{title[this.state.name]}</InputGroup.Text>
        </InputGroup.Prepend>
        </Col>
        <Col className={"d-flex"} lg={6} md={6} >
        {this.state.readOnly?<FormControl
             as={this.state.as}
             type=" "
             min={this.state.min}
             max={this.state.max}
             step={this.state.step}
             pattern={this.state.pattern}
             className={this.state.className}
             plaintext={this.state.plaintext}
             value={this.state.value}
             custom={this.state.custom}
             name={this.state.name}
             readOnly={this.state.readOnly}
             title={desc[this.state.name]}
             onChange={this.inputList.onFormInputChange}
             required={this.state.required}
        />   
        :
        <FormControl
             as={this.state.as}
             type={this.state.type}
             min={this.state.min}
             max={this.state.max}
             step={this.state.step}
             pattern={this.state.pattern}
             className={this.state.className}
             plaintext={this.state.plaintext}
             custom={this.state.custom}
             value={this.state.value}
             name={this.state.name}
             readOnly={this.state.readOnly}
             title={desc[this.state.name]}
             onChange={this.inputList.onFormInputChange}
             required={this.state.required}
        >
            {this.state.type==="select"?window['translations']().option[this.state.name].map((element)=>{
                var keylist=Object.keys(element);
                var rett=[];
                for(var key of keylist){
                    rett.push(<option key={key}value={key}>{element[key]}</option>);
                }
                return(rett);
            }):null}
            
        </FormControl>
        
        }
        {this.state.unit!==undefined?<InputGroup.Append className={"pl-1 pr-2 pt-2 unit"}>{this.state.unit}</InputGroup.Append>:null}
        </Col>
        </Row>
    </InputGroup>);
    }
    
}

export default inputField;

