import * as React from 'react';
import Popup from 'reactjs-popup';
import '../css/popup.css'; 

/**
 * this class is used whenever the user wants to do an action with risky consequences on the site data.
 * It is a popup, composed of a prevention message and two buttons used to confirm the action or cancel it.
 */
function warningsPopup(props) {
    const warningsMessage = window["translations"]().warnings_message;
    return (
        <Popup
            open={props.visible}
            closeOnDocumentClick
            onClose={props.closeWarningsPopup}
            modal
        >
            <div className="warningsPopup">
                <center>
                    <div>
                        <h1>{warningsMessage["warning"]}</h1>
                        <p>{warningsMessage[props.type]["message"]}</p>
                        <strong><p>{warningsMessage[props.type]["question"]}</p></strong>
                    </div>
                    <div>
                        <button className={"btn btn-success mr-2"} onClick={props.handleAction}>{warningsMessage["rep1"]}</button>                      
                        <button className={"btn btn-danger mr-2"} onClick={props.closeWarningsPopup}>{warningsMessage["rep2"]}</button>
                    </div>
                </center>
            </div>
        </Popup>
    );
}

export default warningsPopup;