import * as React from 'react';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import { Button } from 'reactstrap';
import axios from 'axios';
import 'react-tabs/style/react-tabs.css';
import './css/inputList.css';
import InputField from './inputField';
import WarningsPopup from './popups/warningsPopup';
import './css/tabs.css';
import Aladin from './Aladin'
import { faBullseye, faClock} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import env from './dataJson/environment.json';
import equ from './dataJson/equipment.json';
import acq from './dataJson/acquisition.json';
import res from './dataJson/result.json';
import response from './dataJson/dataset_test.json';


import { Col,Row, Card} from 'react-bootstrap';
import Charts from "./charts"; 

class inputList extends React.Component {
    
    constructor(props) {
        super(props);
        this.urlAPI=props.urlAPI;
        this.chart = new Charts({urlAPI:props.urlAPI}); 
        this.myRef=React.createRef();
        //We initialize the state the the value we want
        //We got the default settings from the config file in App.js
        //env equ acq res 
        this.state = {
            mounted:false,

            //Initial settings from props
            session_id: this.props.sessionId,
            observation_id:this.props.dateCreationObservation,
            //Settings setups
            settings:this.props.settings,

            //Use for settings state and errors messages
            errorMsg: '',
            stateMsg: '',
            tabIndex: 0,
            
            //Fields state setups
            fields: {env,equ,acq,res},
            fieldsname:["environment","equipment","acquisition","result"],

            //Component list
            component_list:{},
            component_ref:[],
            
            //Default
            observation_type:"visual",
            target_type:"stars",

            //reponse coords
            coords:"",
            coordT:"",
            
            //popup visibility
            popupVisible: false,
            acqTarget: false,   
            acqTime: true,              
            

            //This will disable inputs when the observation has already been saved
            saved: false,
        };
        //Default value path
        for(var setting in this.state.settings){
            this.state[setting]=this.state.settings[setting];
        }
        //Default value json files
        var index=0;
        var keylist=this.state.fieldsname;
        for(var element in this.state.fields){
            this.state.component_list[keylist[index]]=[];
            var tempElement=this.state.fields[element][keylist[index]];
            var length=tempElement.length;
            index++;
            for(var i=0;i<length;i++){
                if(tempElement[i].name!==undefined && tempElement[i].balise===undefined){
                    var temp=tempElement[i].name;
                    if(!(this.state[temp]!==undefined)){
                        this.state[temp]=undefined;
                    }
                }
            }
        } 
        
    }
    /***
     * Create components in the state
     */
    createComponents= ()=>{
        var arr={};
        var refs=[];
        // For each element, an input field is created
        for(var element in this.state.fields){
            var list=this.state.fields[element];
            var name_field=Object.keys(list)[0];
            list=this.state.fields[element][name_field];
            arr[name_field]=[];
            for(var input in list){
                input=list[input];
                var myref=React.createRef();
                var component=<InputField
                            ref={myref} 
                            key={input.name} 
                            input={input} 
                            inputList={this}
                            observation_type={this.state.observation_type}
                            value={this.state[input.name]}/>
                arr[Object.keys(this.state.fields[element])[0]].push(component);
                refs.push(myref);
            }
        }
        this.setState({component_ref:refs});
        this.setState({component_list:arr});
    }
    /**
     * Component Did mount to create fields
     * */
    componentDidMount(){
        this.setState({mounted:true});
        this.createComponents();
    }

    /**
     * Change fields when observation type is update
    */
    componentDidUpdate(k,preState){

        //Refresh the input list mostly for translations
        this.state.component_ref.forEach((ref)=>{
            if(ref.current!==null && ref.current.mounted){
                ref.current.setState({refresh:!ref.current.state.refresh});
            }
        });
        //On update of state, set all input with matching state
        if(preState!==this.state){
            this.state.component_ref.forEach((item)=>{
                if(item.current!==null){
                    if(preState[item.current.state.name]!==this.state[item.current.state.name])
                        item.current.setState({value:this.state[item.current.state.name]});
                    if((preState.ra_target_j2000!==this.state.ra_target_j2000) ||(preState.dec_target_j2000!==this.state.dec_target_j2000))
                        this.myRef.current.handleSkyMapTarget(
                            this.state.ra_target_j2000===undefined?0:this.state.ra_target_j2000,
                            this.state.dec_target_j2000===undefined?0:this.state.dec_target_j2000);
                }
           });
        }
        //Update when changing observation mode
        if(preState.observation_type!==this.state.observation_type){
            this.state.component_ref.forEach((item)=>{
                if(item.current!==null){
                    item.current.setState({observation_type:this.state.observation_type});
                }  
            });  
        }      
    }


    /**
     * Handle form's change, works only with first level variable in state
     * @param target
     * @returns {Promise<void>}
     */
    onFormInputChange = async ({ target }) => {
        let state = this.state;
        await this.setState({
            ...state,
            [target.name]: target.value
        });
    };

    /**
     * this returns the min date the user can go, which is the creation date of the observation
     * @returns {string}
     */
    getMinDate = () => {
        let date = this.props.dateCreationObservation
        return this.chart.dateFormat(date, "y-m-d"); 
    }
    
    /**
     * Checks that all required fields are filled in
     * @return true if all required fields are filled in, false if not
     */
    checkRequired = () => {
        var ret = true;
        var title=window["translations"]().title;
        var listRequired=document.querySelectorAll("[required]");
        for(var i=0;i<listRequired.length;i++){
            if(listRequired[i].value===""){
                this.setState({errorMsg:title.errorRequired});
                ret = false;
                document.getElementById("errorMsg").hidden = false;
                document.getElementById("successMsg").hidden = true;
            }
        }
        return ret;
    }

    /**
     * Checks that all recommanded fields are filled in
     * @return true if all recommanded fields are filled in, false if not
     */
    checkRecommanded = () => {
        var ret = true;
        var listRecommanded=document.getElementsByClassName("recommanded");
        for (var i=0; i < listRecommanded.length; i++) {
            if ( listRecommanded[i].value === "" ) {
                ret = false;
            }
        }
        return ret;
    }

    /**
     * Get the data from the api and load it into the state
     * Executed when the user click on LOAD DATA
     * @returns {Promise<void>}
     */
    /*
    
    getData = async () => {
        let object = this
        let state = this.state;
        var title=window["translations"]().title;
        try {
            await object.setState({
                ...state,
                stateMsg: title.stateLoading,
                errorMsg: ""
            })
            let response;
            var inputs = document.getElementsByTagName('input');
            response = await axios.get(this.urlAPI+'/get/environment');
            if (response !== undefined) {
                for (var i=0; i<inputs.length; ++i) {
                    console.log(inputs[i]);
                    if(inputs[i].name === "elevation"){
                        this.setState({elevation:response.data.elevation});
                    }
                    if(inputs[i].name === "latitude"){
                        this.setState({latitude:response.data.latitude});
                    }
                    if(inputs[i].name === "longitude"){
                        this.setState({longitude:response.data.longitude});
                    }
                    if(inputs[i].name === "sky_meteo"){
                        this.setState({sky_meteo:response.data.sky});
                    }
                    if(inputs[i].name === "humidity_meteo"){
                        this.setState({humidity_meteo:response.data.humidity});
                    }
                }
                await object.setState({
                    ...state,
                    stateMsg: title.stateLoading,
                    errorMsg: ""
                })
                document.getElementById("errorMsg").hidden = true;
                document.getElementById("successMsg").hidden = false; 
            }

            response = undefined;
            state = this.state;
            response = await axios.get(this.urlAPI+'/get/equipment');
            if (response !== undefined) {
                console.log(response);
                for (i=0; i<inputs.length; ++i) {
                    if(inputs[i].name === "mount"){
                        this.setState({mount:response.data.mount});
                    }
                    if(inputs[i].name === "optic_aperture"){
                        this.setState({optic_aperture:response.data.TELESCOPE_APERTURE});
                    }
                    if(inputs[i].name === "optic_focal_length"){
                        this.setState({optic_focal_length:response.data.TELESCOPE_FOCAL_LENGTH});
                    }
                    if(inputs[i].name === "ccd_name"){
                        this.setState({ccd_name:response.data.mainCCDname});
                    }
                    if(inputs[i].name === "ccd_temperature"){
                        this.setState({ccd_temperature:response.data.temperature});
                    }
                    if(inputs[i].name === "ccd_size"){
                        this.setState({ccd_size:response.data.CCD_PIXEL_SIZE});
                    }
                    if(inputs[i].name === "ccd_pixel"){
                        this.setState({ccd_pixel:response.data.CCD_PIXEL});
                    }
                    if(inputs[i].name === "ccd_bits_per_pixel"){
                        this.setState({ccd_bits_per_pixel:response.data.CCD_BITSPERPIXEL});
                    }
                    if(inputs[i].name === "ccd_frame_width"){
                        this.setState({ccd_frame_width:response.data.WIDTH});
                    }
                    if(inputs[i].name === "ccd_frame_height"){
                        this.setState({ccd_frame_height:response.data.HEIGHT});
                    }
                    if(inputs[i].name === "ccd_horizontal_binning"){
                        this.setState({ccd_horizontal_binning:response.data.HOR_BIN});
                    }
                    if(inputs[i].name === "ccd_vertical_binning"){
                        this.setState({ccd_vertical_binning:response.data.VER_BIN});
                    }
                    if(inputs[i].name === "optic_guide_aperture"){
                        this.setState({optic_guide_aperture:response.data.GUIDER_APERTURE});
                    }
                    if(inputs[i].name === "optic_guide_focal_length"){
                        this.setState({optic_guide_focal_length:response.data.GUIDER_FOCAL_LENGTH});
                    }
                }
            }
            response = undefined;
            state = this.state;
            response = await axios.get(this.urlAPI+'/get/acquisition');
            var dec;
            var ra;
            if (response !== undefined) {
                console.log(response);
                ra=response.data.j2000_ra;
                dec=response.data.j2000_dec;
                for (i=0; i<inputs.length; ++i) {
                    if(inputs[i].name === "ra_target_j2000"){
                        this.setState({ra_target_j2000:response.data.j2000_ra});
                    }
                    if(inputs[i].name === "dec_target_j2000"){
                        this.setState({dec_target_j2000:response.data.j2000_dec});
                    }
                }
                await object.setState({
                    ...state,
                    stateMsg: title.stateLoaded
                })
                document.getElementById("errorMsg").hidden = true;
                document.getElementById("successMsg").hidden = false; 
            }
            this.myRef.current.handleSkyMapCreation(ra,dec)

        } catch (e) {
            object.setState({
                ...state,
                errorMsg: title.errorIndiserv,
                stateMsg: ""
            })
            document.getElementById("errorMsg").hidden = false;
            document.getElementById("successMsg").hidden = true; 
        }
    }

    */

     /**
     * Get the data from the api and load it into the state
     * Executed when the user click on LOAD DATA
     * @returns {Promise<void>}
     */
    getData = async () => {
        let object = this
        let state = this.state;
        var title = window["translations"]().title;
        await object.setState({
            ...state,
            stateMsg: title.stateLoading,
            errorMsg: ""
        })
        var inputs = document.getElementsByTagName('input');
        if (response !== undefined) {
            for (var i = 0; i < inputs.length; ++i) {
                if (inputs[i].name === "elevation") {
                    this.setState({
                        elevation: response.elevation
                    });
                }
                if (inputs[i].name === "latitude") {
                    this.setState({
                        latitude: response.latitude
                    });
                }
                if (inputs[i].name === "longitude") {
                    this.setState({
                        longitude: response.longitude
                    });
                }
                if (inputs[i].name === "sky_meteo") {
                    this.setState({
                        sky_meteo: response.sky_meteo
                    });
                }
                if (inputs[i].name === "humidity_meteo") {
                    this.setState({
                        humidity_meteo: response.humidity_meteo
                    });
                }
            }
        }

        state = this.state;
        if (response !== undefined) {
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].name === "mount") {
                    this.setState({
                        mount: response.mount
                    });
                }
                if (inputs[i].name === "optic_aperture") {
                    this.setState({
                        optic_aperture: response.optic_aperture
                    });
                }
                if (inputs[i].name === "optic_focal_length") {
                    this.setState({
                        optic_focal_length: response.optic_focal_length
                    });
                }
                if (inputs[i].name === "ccd_name") {
                    this.setState({
                        ccd_name: response.ccd_name
                    });
                }
                if (inputs[i].name === "ccd_temperature") {
                    this.setState({
                        ccd_temperature: response.ccd_temperature
                    });
                }
                if (inputs[i].name === "ccd_size") {
                    this.setState({
                        ccd_size: response.ccd_size
                    });
                }
                if (inputs[i].name === "ccd_pixel") {
                    this.setState({
                        ccd_pixel: response.ccd_pixel
                    });
                }
                if (inputs[i].name === "ccd_bits_per_pixel") {
                    this.setState({
                        ccd_bits_per_pixel: response.ccd_bits_per_pixel
                    });
                }
                if (inputs[i].name === "ccd_frame_width") {
                    this.setState({
                        ccd_frame_width: response.ccd_frame_width
                    });
                }
                if (inputs[i].name === "ccd_frame_height") {
                    this.setState({
                        ccd_frame_height: response.ccd_frame_height
                    });
                }
                if (inputs[i].name === "ccd_horizontal_binning") {
                    this.setState({
                        ccd_horizontal_binning: response.ccd_horizontal_binning
                    });
                }
                if (inputs[i].name === "ccd_vertical_binning") {
                    this.setState({
                        ccd_vertical_binning: response.ccd_vertical_binning
                    });
                }
                if (inputs[i].name === "optic_guide_aperture") {
                    this.setState({
                        optic_guide_aperture: response.optic_guide_aperture
                    });
                }
                if (inputs[i].name === "optic_guide_focal_length") {
                    this.setState({
                        optic_guide_focal_length: response.optic_guide_focal_length
                    });
                }
            }
        }

        state = this.state;
        var dec;
        var ra;
        if (response !== undefined) {
            ra = response.ra_ref_star_j2000;
            dec = response.dec_ref_star_j2000;
            for (i = 0; i < inputs.length; ++i) {
                if (inputs[i].name === "ra_target_j2000") {
                    this.setState({
                        ra_target_j2000: response.ra_ref_star_j2000
                    });
                }
                if (inputs[i].name === "dec_target_j2000") {
                    this.setState({
                        dec_target_j2000: response.dec_ref_star_j2000
                    });
                }
            }
        }
        this.myRef.current.handleSkyMapCreation(ra, dec);
    }


    /**
     * This generate the json with the good format and send it to the api in order to be saved in the Database
     * @returns {Promise<void>}
     */
    saveData = async () => {
        if (this.checkRequired()){        
            if(this.checkRecommanded())
                this.putData();
            else
                this.openPopup();
        }
    }
    
    /**
     * Put the data in the json for the data base
     */
    putData = async () => {
        if (this.state.popupVisible === true ) {
            await this.closePopup();
        }
        var inputs = document.getElementsByTagName('input');
        for (var i=0; i<inputs.length; ++i) {
            if(inputs[i].value!==""){
                inputs[i].style.border="green solid 2px";
                inputs[i].style.borderRight="green solid 5px";
            }
        }
        var title=window["translations"]().title;
        let object = this;
        let state = this.state;
        //Format the json document we'll store into the database
        var jsonToSave ={};
        var index=0;
        var keylist=this.state.fieldsname;
        var balise;
        for(var element in this.state.fields){
            var tempElement=this.state.fields[element][keylist[index]];
            var length=tempElement.length;
            index++;
            for(i=0;i<length;i++){
                if(tempElement[i].balise!==undefined && tempElement[i].name_bdd!==undefined){
                    balise=tempElement[i].name_bdd;
                    jsonToSave[balise]={};
                }
                if(tempElement[i].name!==undefined && tempElement[i].balise===undefined){
                    var temp=tempElement[i].name;
                    if(balise===undefined){
                        jsonToSave[temp]=this.state[temp];
                    }
                    else{
                        jsonToSave[balise][temp]=this.state[temp];
                    }                    
                }
            }
            balise=undefined;
        }
        //Control the existence of required input
        //Check if the observation was saved before in order to know which route to use, one is for new document and the other one is used to update an existant document
        if (!this.state.saved) {
            //Save a new observation
            axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
            axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
            axios.post(this.urlAPI+'/post/form', {
                form: jsonToSave
            })
                .then(function (response) {
                    object.setState({
                        ...state,
                        errorMsg: "",
                        stateMsg: title.stateSaved,
                        saved: true
                    })
                    document.getElementById("errorMsg").hidden = true;
                document.getElementById("successMsg").hidden = false; 
                })
                .catch(function (error) {
                    object.setState({
                        ...state,
                        stateMsg: "",
                        errorMsg: title.errorSaved,
                    })
                    document.getElementById("errorMsg").hidden = false;
                    document.getElementById("successMsg").hidden = true;
                });
        } else {
            //Update an existant observation
            axios.post(this.urlAPI+'/post/updateForm', {
                form: jsonToSave
            })
                .then(function (response) {
                    object.setState({
                        ...state,
                        errorMsg: "",
                        stateMsg: title.stateUpdated,
                        saved: true
                    })
                    document.getElementById("errorMsg").hidden = true;
                    document.getElementById("successMsg").hidden = false; 
                })
                .catch(function (error) {
                    console.log(error)
                    object.setState({
                        ...state,
                        stateMsg: "",
                        errorMsg: title.errorSaved,
                    })
                    document.getElementById("errorMsg").hidden = false;
                    document.getElementById("successMsg").hidden = true; 
                });
        }
    }

    /**
     * open the popup
     */
    openPopup = () => {
        this.setState({popupVisible: true});
    }

    /**
     * close the popup
     */
    closePopup = async () => {
        await this.setState({popupVisible: false});
    }

    /**
     * calculateCoords : calculate the coordinates if necessary
     */
    calculateCoords = async () =>{
        await this.getObjCoords("object_name");
        if (this.state.observation_type === 'spectro') {
            await this.getObjCoords("name_ref_star");
        }
    }

    /**
     * displayCurrentTime : returns the current date and initializes it to the observation end time
     */
    displayCurrentTime = async () =>{
        var today = new Date();
        var time = (today.getHours()<10?"0"+today.getHours():today.getHours())+ ':' + (today.getMinutes()<10?"0"+today.getMinutes():today.getMinutes());
        document.getElementsByName('end_time')[0].value = time;
        this.setState({end_time:time});
    }

    /**
     * getObjCoords : Returns the coordinates of the object whose name is marked in the indicated field name_ref_star and object_name
     */
    getObjCoords = async (field) => {
        let state = this.state;
        let name;
        var title=window["translations"]().title;
        var inputs = document.getElementsByTagName('input');
        for (var i=0; i<inputs.length; ++i) {
            if(inputs[i].name === field) {
                name = inputs[i].value;
            }
        }
        await this.setState({
            ...state,
            stateMsg: title.stateLoading,
            errorMsg: ""
        })
        if ( name !== "" ) {
            try {
                let url = this.urlAPI+'/getobjcoords/'+ name;
                let response;
                response = await axios.get(url);
                if (response !== undefined) {
                    await this.setState({
                        ...state,
                        coords:{...this.state.coords,[field]:response},
                        stateMsg: title.stateLoadedCoord,
                        errorMsg: ""
                    })
                    document.getElementById("errorMsg").hidden = true;
                    document.getElementById("successMsg").hidden = false;
                }
            } catch(e) {
                this.setState({
                    ...state,
                    errorMsg: title.errorCoordNotFound,
                    stateMsg: ""
                })
                document.getElementById("errorMsg").hidden = false;
                document.getElementById("successMsg").hidden = true;
            } finally {
                if(field==="name_ref_star"){
                    this.initJ2000();
                }
                else{
                    this.initTarget();
                }
            }
        } else {
            this.setState({
                ...state,
                errorMsg: title.errorCoordEmpty,
                stateMsg: ""
            })
            document.getElementById("errorMsg").hidden = false;
            document.getElementById("successMsg").hidden = true;
        }
    }

    /**
     * this method is used to put the value of the coords in the corresponding input fields
     */
    initJ2000 = () => {
        var obj=this.state.coords.name_ref_star;
        if (obj === undefined) {
            console.log("Objet inexistant!")
            return;
        }
        this.setState({ra_ref_star_j2000:obj.data[0]});               
        this.setState({dec_ref_star_j2000:obj.data[1]});                   
        this.setState({spectral_type_ref_star:obj.data[2]});
        var trad = window["translations"]().info;
        document.getElementById('rs_ra_info').innerHTML = "" + obj.data[0];
        document.getElementById("rs_dec_info").innerHTML = ""+obj.data[1];
        if (obj.data[2] !== "") {
            document.getElementById('title_rs_sptype_info').innerHTML = trad.sptype;
            document.getElementById("rs_sptype_info").innerHTML = "" + obj.data[2];
        } else document.getElementById('rs_sptype_info').innerHTML = trad.noSpecType;
        document.getElementById("rs_rv_value_info").innerHTML = "" + obj.data[3];
        document.getElementById("rs_flux_v_info").innerHTML = "" + obj.data[4];
        if (obj.data[5] !== "") {
            document.getElementById('title_rs_mt_info').innerHTML = trad.mp_type;
            document.getElementById("rs_mt_info").innerHTML = "" + obj.data[5];
        } else document.getElementById('rs_mt_info').innerHTML = trad.noMorphType;     
        document.getElementById( "rs_otype_info").innerHTML = "" + obj.data[6];
        document.getElementById("rs_dim_info").innerHTML = "" + obj.data[7];
        
    }

    /**
     * this method is used to put the value of the coords in the corresponding input fields for the target field
     */
    initTarget = () => {
        var obj=this.state.coords.object_name;
        if(this.state.ra_target_j2000!==undefined && this.state.dec_target_j2000!==undefined){
            this.myRef.current.handleSkyMapTarget(this.state.ra_target_j2000,this.state.dec_target_j2000);
        }
        if (obj === undefined) {
            console.log("Objet inexistant!")
            return;
        }
        this.setState({ra_target_j2000:obj.data[0]});
        this.setState({dec_target_j2000:obj.data[1]});
        var trad = window["translations"]().info;
        document.getElementById("ra_info").innerHTML = "" + obj.data[0];
        document.getElementById("dec_info").innerHTML = "" + obj.data[1];
        if (obj.data[2] !== "") {
            document.getElementById('title_sptype_info').innerHTML = trad.sptype;
            document.getElementById("sptype_info").innerHTML = "" + obj.data[2];
        } else document.getElementById('sptype_info').innerHTML = trad.noSpecType;
        document.getElementById("rv_value_info").innerHTML = "" + obj.data[3];        
        document.getElementById( "flux_v_info").innerHTML = "" + obj.data[4];
        if (obj.data[5] !== "") {
            document.getElementById('title_mt_info').innerHTML = trad.mp_type;
            document.getElementById("mt_info").innerHTML = "" + obj.data[5];
        } else document.getElementById('mt_info').innerHTML = trad.noMorphType;
        document.getElementById("otype_info").innerHTML = "" + obj.data[6];
        document.getElementById( "dim_info").innerHTML = "" + obj.data[7];
    }
    
    render() {

        var button=window["translations"]().button;
        var title=window["translations"]().title;
        var description=window["translations"]().description;

        return (
            <div className="formContent">
                <link rel='stylesheet' href='https://aladin.u-strasbg.fr/AladinLite/api/v2/latest/aladin.min.css' />
                <script type='text/javascript' src='https://code.jquery.com/jquery-1.9.1.min.js'
                    charSet='utf-8'></script>

                <form noValidate onSubmit={(e) => {
                    e.preventDefault();
                }}>
                    {/* This is the bar with buttons Load Data and Verify and Save, and all the messages */}
                    <Row className="rowButtons">
                        <Col className="colLoad">
                        <button type="button" className={"btn btn-primary load"} onClick={this.getData}>{button["loadData"]}
                        </button>
                        </Col>
                        <Col className="colSubmit">
                        <button type="submit" className={"btn btn-info verify"} onClick={this.saveData}>{button["verify"]}
                        </button>
                        </Col>
                        <Col >
                        <span id="errorMsg" className="disp" style={{ color: "red" }} hidden>{this.state.errorMsg}</span>
                        <span id="successMsg" className="disp" style={{ color: "green" }} hidden>{this.state.stateMsg}</span>
                        </Col>
                    </Row>

                    <Row  className="my-3">
                        <Col className="colForm w-100 d-block">
                            <Tabs id="tab" className="w-100 justify-content-center corner">
                                <Card className="tabList">
                                    <TabList id="in">
                                        <Tab tabFor="env" onClick={()=>{this.setState({acqTarget: false, acqTime: true})}}>{title['environment']}</Tab>
                                        <Tab tabFor="equ" onClick={()=>{this.setState({acqTarget: false,acqTime: false})}}>{title['equipment']}</Tab>
                                        <Tab tabFor="acq" onClick={()=>{this.setState({acqTarget: true, acqTime: false})}}>{title['acquisition']}</Tab>
                                        <Tab tabFor="res" onClick={()=>{this.setState({acqTarget: false, acqTime: false})}}>{title['result']}</Tab>
                                        {this.state.acqTarget?
                                        <Button className={"btn btn-warning text-white btn-get"} alt={description["coords"]} title={description["coords"]} onClick={this.calculateCoords}><FontAwesomeIcon icon={faBullseye}/></Button>
                                        :null}
                                        {this.state.acqTime?
                                        <Button className={"btn btn-warning text-white btn-get"} alt={description["clock"]} title={description["clock"]} onClick={this.displayCurrentTime}><FontAwesomeIcon icon={faClock}/></Button>
                                        :null}
                                    </TabList>
                                </Card>
                                <TabPanel tabId="env">
                                    <Card className="tabContent">
                                        <Card.Body className="environmentTabContent">
                                            {this.state.component_list.environment}              
                                        </Card.Body>
                                        </Card>
                                </TabPanel>
                                <TabPanel tabId="equ">
                                    <Card className="tabContent">
                                        <Card.Body className="equipmentTabContent">
                                            {this.state.component_list.equipment}   
                                        </Card.Body>
                                    </Card>
                                 </TabPanel>
                                <TabPanel tabId="acq">
                                    <Card className="tabContent">
                                        <Card.Body className="acquisitionTabContent">
                                            {this.state.component_list.acquisition}  
                                        </Card.Body>
                                    </Card>
                                </TabPanel>
                                <TabPanel tabId="res">
                                    <Card className="tabContent">
                                        <Card.Body className="resultTabContent">
                                            {this.state.component_list.result} 
                                        </Card.Body>
                                    </Card>
                                </TabPanel>

                               
                            </Tabs>
                        </Col>
                        {/* This is the aladin map div */}
                        <Col className="colAladin">
                            <Aladin ref={this.myRef}
                            visibleRefStar = {this.state.observation_type !== 'spectro'?true:false} 
                            observationId={this.state.observation_id}
                            />
                        </Col>

                    </Row>
                </form>
                <WarningsPopup
                    visible={this.state.popupVisible}
                    closeWarningsPopup={this.closePopup}
                    handleAction={this.putData}
                    type="emptyRecommanded"
                />
                <div>
                </div>
            </div>
        );
    }
}

export default inputList;
